package com.kgc.controller;



import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kgc.entity.Goods;
import com.kgc.entity.PageBean;
import com.kgc.service.GoodsService;

@Controller
@RequestMapping("goods")
public class GoodsController {
	
	@Autowired
	private GoodsService goodsService;
	
	@RequestMapping("main")
	@ResponseBody
	public PageBean<Goods>  main(@RequestParam(value="currentPage",defaultValue="1",required=false)int currentPage,HttpSession session){
		
		PageBean<Goods> pageBean = goodsService.findByPage(currentPage);
		session.setAttribute("totalPage", pageBean.getTotalPage());
		return pageBean;
	}
	
	@RequestMapping("main2")
	@ResponseBody
	public List<Goods>  main2(int currentPage){
		PageBean<Goods> pageBean = goodsService.findByPage(currentPage);
		List<Goods> list = pageBean.getLists();
		return list;
	}
	
	@RequestMapping("queryGoods")
	@ResponseBody
	public List<Goods> queryGoods(String name,HttpServletRequest request){
		return goodsService.queryGoods(name);
	}
	
	@RequestMapping("selectGoods")
	@ResponseBody
	 public Goods selectGoods(int id){
		 return goodsService.selectGoods(id);
	 }
	
	
}

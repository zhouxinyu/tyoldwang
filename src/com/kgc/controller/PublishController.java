package com.kgc.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.kgc.entity.Goods;
import com.kgc.entity.Publish;
import com.kgc.entity.Sort;
import com.kgc.entity.User;
import com.kgc.service.PublishService;
import com.kgc.util.MyUtils;

/**
 * 卖家后端管理
 * @author lenovo
 *
 */
@Controller
@RequestMapping("publish")

public class PublishController {
	
	@Autowired
	private PublishService publishService;
	
	/**
	 * 获得分类
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getSorts")
	public List<Sort> getSort(){
		return publishService.getSort();
	}
	
	
	/**
	 * 提交卖家发布商品
	 * @param publish
	 * @throws IOException 
	 */
	@RequestMapping("create")
	public String publish(HttpServletRequest request,MultipartFile file) throws IOException{
		
		HttpSession session = request.getSession();
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String imgName = UUID.randomUUID().toString().replaceAll("-", "");
		String ext = FilenameUtils.getExtension(file.getOriginalFilename());
		String url = request.getSession().getServletContext().getRealPath("/images/upload");
		
		file.transferTo(new File(url+"/"+imgName + "." + ext));
		//源文件地址
		File source=new File(url+"/"+imgName + "." + ext);
		//目的文件地址
		File dest=new File("D:/Workspaces/MyEclipse 2017 CI/SecondHandSystem/WebRoot/images/upload/"+imgName + "." + ext);
		MyUtils.CopyImg(source, dest);
		Goods newpublish = new Goods();
		newpublish.setG_name(request.getParameter("name"));
		newpublish.setG_price(Double.valueOf(request.getParameter("price")));
		newpublish.setSort_id(Integer.valueOf(request.getParameter("sort")));
		newpublish.setText(request.getParameter("text"));
		newpublish.setCreate_time(dateFormat.format( now ));
		User user = (User)session.getAttribute("user");
		newpublish.setM_id(user.getU_id());
		newpublish.setG_img("images/upload/"+imgName + "." + ext);
		newpublish.setZt_id(3);
		publishService.publish(newpublish);
		System.out.println("发布成功！");
		return "redirect:/Personal_information/Assess.jsp";
	}
	
	@RequestMapping("getpublish")
	@ResponseBody
	public List<Goods> getpublish(int u_id){
		return publishService.getpublish(u_id);
		
	}
	@RequestMapping("getingpublish")
	@ResponseBody
	public List<Goods> getingpublish(int u_id){
		return publishService.getingpublish(u_id);
		
	}
	@RequestMapping("getedpublish")
	@ResponseBody
	public List<Goods> getedpublish(int u_id){
		return publishService.getedpublish(u_id);
		
	}
	@RequestMapping("getnopublish")
	@ResponseBody
	public List<Goods> getnopublish(int u_id){
		return publishService.getnopublish(u_id);
		
	}
	@RequestMapping("getpublishjie")
	@ResponseBody
	public List<Goods> getpublishjie(int u_id){
		return publishService.getpublishjie(u_id);
		
	}
	@RequestMapping("quxiaopublish")
	@ResponseBody
	public boolean quxiaopublish(int shen_id){
		return publishService.quxiaopublish(shen_id);
		
	}
}

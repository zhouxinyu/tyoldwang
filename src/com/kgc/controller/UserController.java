package com.kgc.controller;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kgc.entity.User;
import com.kgc.service.UserService;
import com.kgc.util.RandomNumber;
import com.kgc.util.SmsDemo;

@Controller
@RequestMapping("user")
public class UserController {
	
	@Autowired
	private UserService userLoginService;
	
	@RequestMapping("userLogin")
	@ResponseBody
	public boolean login(String phone, String password, HttpServletRequest req,HttpServletResponse response){
		User user = userLoginService.login(phone, password);
		if(user != null){
			
			req.getSession().setAttribute("user", user);
			if(req.getParameter("zidong") != null&&req.getParameter("zidong")!=""){
				Cookie cookie = new Cookie("userinfo", user.getU_phone());
				cookie.setMaxAge(30*24*3600);//设置Cookie有效期为30天
				cookie.setPath("/");
				response.addCookie(cookie);
			}
			return true;
		}			
		return false;
	}
	
	@RequestMapping("getcookies")
	@ResponseBody
	public boolean getCookies(HttpServletRequest req){
		User user = (User) req.getSession().getAttribute("user");
		if(user==null){
			Cookie[] cs=req.getCookies();
			String v=null;
			if(cs!=null){
				for(int i=0;i<cs.length;i++){
					if(cs[i].getName().equals("userinfo")){  //获取名称为username的Cookie对象值
                        v=cs[i].getValue();
                    }
                }
            }
			if(v!=null){
				User user2 = userLoginService.queryByPhone(v);
                req.getSession().setAttribute("user",user2);
                System.out.println(user2.getU_name()+"自动登录成功！");
                return true;
			}
		}
		return false;
	}
	
	/**
	 * 短信验证
	 * @param phone
	 * @param req
	 */
	@RequestMapping("code")
	@ResponseBody
	public boolean getVerificationCode(String phone,HttpServletRequest req, HttpServletResponse resp){
		try {
			RandomNumber randomNumber = new RandomNumber();
			HttpSession session =req.getSession();
			String codeNum=randomNumber.sixNumber();
	//	if(SmsDemo.sendSms(phone,codeNum)){
				session.setAttribute("phone", phone);
				session.setAttribute("code", codeNum);
				System.out.println(codeNum);
				return true;
	//	}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 通过手机号找用户
	 * @param phone
	 * @return
	 */
	@RequestMapping("queryByPhone")
	@ResponseBody
	public boolean queryByPhone(String code,HttpServletRequest req){
		HttpSession session = req.getSession();
		String phone = (String) session.getAttribute("phone");
		String serverCode = (String) session.getAttribute("code");
		if(serverCode!=""&&serverCode.equals(code)){
			System.out.println("验证码正确！");
			User user = userLoginService.queryByPhone(phone);
			return user==null?true:false;
		}else{
			return false;
		}
	}
	
	
	/**
	 * 增加用户
	 * @param phone
	 * @param password
	 * @return
	 */
	@RequestMapping("addUser")
	@ResponseBody
	public boolean add(String password,HttpServletRequest req){
		HttpSession session = req.getSession();
		String phone = (String)session.getAttribute("phone");
		RandomNumber randomNumber  =new RandomNumber();
		String userName= "用户"+randomNumber.fourNumber();
		User user = new User();
		user.setU_phone(phone);
		user.setU_password(password);
		user.setU_name(userName);
		user.setU_address("中国");
		user.setU_touxiang("images/user/default.png");
		boolean f= userLoginService.addUser(user);
		if(f){
			System.out.println("用户创建成功！");
			session.removeAttribute("phone");
			session.removeAttribute("code");
			return  true;
		}
		return false;
	}
	
	@RequestMapping("exitUser")
	public String exitUser(HttpServletRequest req){
		HttpSession session = req.getSession();
		session.removeAttribute("user");
		Cookie cookie= new Cookie("userinfo", null);
		cookie.setMaxAge(0);
		cookie.setPath(req.getContextPath());
		System.out.println("用户已退出");
		return "redirect:/shouye.jsp";
	}
	
	@RequestMapping("getuser")
	@ResponseBody
	public User getNameByG_id(int g_id){
		
		return userLoginService.getNameByG_id(g_id);
	}
	
	@RequestMapping("UpdatePwd")
	@ResponseBody
	public boolean UpdatePwd(String phone,String oldpassword,String newpassword){
		return userLoginService.UpdatePwd(phone,oldpassword,newpassword);
		
	}
	
	@RequestMapping("yzPhone")
	@ResponseBody
	public int yzPhone(String phone){
		int row = userLoginService.yzPhone(phone);
		if (row>0) {
			return 11;
		}
		return 22;
	}
	
	@RequestMapping("yz")
	@ResponseBody
	public int yz(String phone,String code,HttpServletRequest req){
		HttpSession session = req.getSession();
		int row = userLoginService.yzPhone(phone);
		String serverCode = (String) session.getAttribute("code");
		if (row>0) {
			if(serverCode!=""&&serverCode.equals(code)){
				System.out.println("验证码正确！");
				return 11;
			}else{
				return 22;
			}
		}
		return 22;
	}
	
	@RequestMapping("updatePaswd")
	@ResponseBody
	public boolean updatePaswd(String pwd,String phone,HttpServletRequest req){
		return userLoginService.updatePaswd(pwd,phone);
	}
	
	
	@RequestMapping("update")
	@ResponseBody
	public boolean update(User user,HttpServletRequest req){
		HttpSession session = req.getSession();
		User User = (com.kgc.entity.User)session.getAttribute("user");
		User.setU_age(user.getU_age());
		User.setU_name(user.getU_name());
		session.setAttribute("user", User);
		return userLoginService.update(user);
	}
}

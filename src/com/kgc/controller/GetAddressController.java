  package com.kgc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kgc.entity.Address;
import com.kgc.service.GetAddressService;

@Controller
@RequestMapping("Address")
public class GetAddressController {
	@Autowired
	private GetAddressService getaddress;
	
	@RequestMapping("insert")
	@ResponseBody
	public boolean insertaddress(Address address){
		return getaddress.insertaddress(address);
	};
	
	@RequestMapping("get")
	@ResponseBody
	public List<Address> getaddress(int u_id){
		return getaddress.getaddress(u_id);
	};
	
	@RequestMapping("delete")
	@ResponseBody
	public boolean deleteaddress(Integer gad_id){
		return getaddress.deleteaddress(gad_id);
	};
	
	@RequestMapping("Info")
	@ResponseBody
	public List<Address> getInfo(int u_id){
		return getaddress.getInfo(u_id);
	};

}

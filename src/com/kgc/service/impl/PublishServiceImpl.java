package com.kgc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kgc.entity.Goods;
import com.kgc.entity.Publish;
import com.kgc.entity.Sort;
import com.kgc.mapper.PublishMapper;
import com.kgc.service.PublishService;

@Service
public class PublishServiceImpl implements PublishService {

	@Autowired
	private PublishMapper publishMapper;
	
	/*public boolean publish(Goods goods) {
		
		return publishMapper.publish(goods)==1?true:false;
	}*/

	@Override
	public List<Sort> getSort() {
		return publishMapper.getSort();
	}

	@Override
	public boolean publish(Goods publish) {
		return publishMapper.publish(publish) == 1?true:false;
	}

	@Override
	public List<Goods> getpublish(int u_id) {
		return publishMapper.getpublish(u_id);
	}
	@Override
	public List<Goods> getingpublish(int u_id) {
		return publishMapper.getingpublish(u_id);
	}
	@Override
	public List<Goods> getnopublish(int u_id) {
		return publishMapper.getnopublish(u_id);
	}
	@Override
	public List<Goods> getedpublish(int u_id) {
		return publishMapper.getedpublish(u_id);
	}
	@Override
	public boolean quxiaopublish(int shen_id) {
		return publishMapper.quxiaopublish(shen_id)==1?true:false;
	}

	@Override
	public List<Goods> getpublishjie(int u_id) {
		return publishMapper.getpublishjie(u_id);
	}

}

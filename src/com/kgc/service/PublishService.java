package com.kgc.service;

import java.util.List;

import com.kgc.entity.Goods;
import com.kgc.entity.Publish;
import com.kgc.entity.Sort;

public interface PublishService {
	/**
	 * 卖家提交发布
	 * @return
	 */
	
	boolean publish(Goods publish);
	
	public List<Goods> getpublish(int u_id);
	public List<Goods> getpublishjie(int u_id);
	public List<Goods> getingpublish(int u_id);
	public List<Goods> getnopublish(int u_id);
	public List<Goods> getedpublish(int u_id);
	//取消订单
	public boolean quxiaopublish(int shen_id);
	
	List<Sort> getSort();
	
}

package com.kgc.service;

import com.kgc.entity.User;

public interface UserService {
	
	public User login(String phone,String password);
	
	public User queryByPhone(String phone);
	
	public boolean addUser(User user);
	
	public User getNameByG_id(int g_id);
	
	public boolean UpdatePwd(String phone,String oldpassword,String newpassword);
	
	public int yzPhone(String phone);
	
	public boolean updatePaswd(String pwd,String phone);

	
	public boolean update(User user);
}

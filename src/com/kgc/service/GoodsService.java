package com.kgc.service;

import java.util.List;

import com.kgc.entity.Goods;
import com.kgc.entity.PageBean;

public interface GoodsService {
	

    int selectCount();

    PageBean<Goods> findByPage(int currentPage);
    
    List<Goods> findByPage2(int currentPage);
    
    List<Goods> selectUserList();
    
    List<Goods> queryGoods(String name);
    
    public Goods selectGoods(int id);
    
    boolean updateGoods(int g_id,int zt_id);
}

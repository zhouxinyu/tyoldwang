package com.kgc.service;

import java.util.List;

import com.kgc.entity.Address;

public interface GetAddressService {
	
	public boolean insertaddress(Address address);
	
	
	public List<Address> getaddress(int u_id);
	

	public boolean deleteaddress(Integer gad_id);
	
	public List<Address> getInfo(int u_id);
}

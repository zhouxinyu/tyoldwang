package com.kgc.entity;

public class Address {
	private int gad_id;
	private int u_id;
	private String  u_name;
	private String u_password;
	private String gad_name;
	private String gad_phone;
	private String gad_address;
	private int gad_sex;
	
	


	public int getGad_sex() {
		return gad_sex;
	}


	public void setGad_sex(int gad_sex) {
		this.gad_sex = gad_sex;
	}




	public int getGad_id() {
		return gad_id;
	}


	public void setGad_id(int gad_id) {
		this.gad_id = gad_id;
	}


	public int getU_id() {
		return u_id;
	}


	public void setU_id(int u_id) {
		this.u_id = u_id;
	}


	public String getU_name() {
		return u_name;
	}


	public void setU_name(String u_name) {
		this.u_name = u_name;
	}


	public String getU_password() {
		return u_password;
	}


	public void setU_password(String u_password) {
		this.u_password = u_password;
	}


	public String getGad_name() {
		return gad_name;
	}


	public void setGad_name(String gad_name) {
		this.gad_name = gad_name;
	}


	public String getGad_phone() {
		return gad_phone;
	}


	public void setGad_phone(String gad_phone) {
		this.gad_phone = gad_phone;
	}


	public String getGad_address() {
		return gad_address;
	}


	public void setGad_address(String gad_address) {
		this.gad_address = gad_address;
	}


	@Override
	public String toString() {
		return "Address [gad_id=" + gad_id + ", u_name=" + u_name + ", u_password=" + u_password + ", gad_name="
				+ gad_name + ", gad_phone=" + gad_phone + ", gad_address=" + gad_address + "]";
	}
	
	
	
}

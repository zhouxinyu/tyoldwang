package com.kgc.entity;

public class Trade {
	
	private String out_trade_no;
	private String total_amount;
	private String trade_no="1";
	private String number;
	private int u_id;
	private int g_id;
	private String createtime;
	private int tradestates_id;  // 订单状态（是否支付）
	private String receive_id; // 订单状态（是否收货）
	private Goods goods;
	
	
	
	public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	public int getU_id() {
		return u_id;
	}
	public void setU_id(int u_id) {
		this.u_id = u_id;
	}
	public int getG_id() {
		return g_id;
	}
	public void setG_id(int g_id) {
		this.g_id = g_id;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public int getTradestates_id() {
		return tradestates_id;
	}
	public void setTradestates_id(int tradestates_id) {
		this.tradestates_id = tradestates_id;
	}
	public String getTrade_no() {
		return trade_no;
	}
	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}
	
	public String getReceive_id() {
		return receive_id;
	}
	public void setReceive_id(int receive_id) {
		if(receive_id == 1){
			this.receive_id = "未收货";
		} else if(receive_id == 2){
			this.receive_id = "已收货";
		}
	}
	@Override
	public String toString() {
		return "Trade [out_trade_no=" + out_trade_no + ", total_amount=" + total_amount + ", trade_no=" + trade_no
				+ ", u_id=" + u_id + ", g_id=" + g_id + ", createtime=" + createtime + ", tradestates_id="
				+ tradestates_id + ", receive_id=" + receive_id + "]";
	}

	
}

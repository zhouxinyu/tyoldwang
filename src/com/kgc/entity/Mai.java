package com.kgc.entity;

public class Mai {
	
	private int id;
	private String name;
	private String password;
	private String phone;
	private String address;
	private String nicheng;
	private int age;
	private int sex;
	private String jieshao;
	private String number;
	private String touxiang;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNicheng() {
		return nicheng;
	}
	public void setNicheng(String nicheng) {
		this.nicheng = nicheng;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getJieshao() {
		return jieshao;
	}
	public void setJieshao(String jieshao) {
		this.jieshao = jieshao;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getTouxiang() {
		return touxiang;
	}
	public void setTouxiang(String touxiang) {
		this.touxiang = touxiang;
	}
	@Override
	public String toString() {
		return "Mai [id=" + id + ", name=" + name + ", password=" + password + ", phone=" + phone + ", address="
				+ address + ", nicheng=" + nicheng + ", age=" + age + ", sex=" + sex + ", jieshao=" + jieshao
				+ ", number=" + number + ", touxiang=" + touxiang + "]";
	}
	
	
}

package com.kgc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.kgc.entity.Goods;
import com.kgc.entity.Trade;
import com.sun.org.glassfish.gmbal.ParameterNames;

@Repository
public interface TradeMapper {
	
	int createtrade(@Param("trade")Trade trade);
	
	int updatetrade(@Param("out_trade_no")String out_trade_no,@Param("trade_no")String trade_no,@Param("flg")int flg,@Param("receive_id")int receive_id);
	
	public List<Trade> gettrade(@Param("u_id")int u_id);
	
	public int changetrade(@Param("out_trade_no")String out_trade_no);
	
	public int deletetrade(@Param("out_trade_no")String out_trade_no);
	
	public List<Trade> getedtrade(@Param("u_id")int u_id);
	
	public List<Trade> getnotrade(@Param("u_id")int u_id);
	
	/*
	 * 查询商品Id
	 */
	public int idByGoods(@Param("out_trade_no") String out_trade_no);
	 /*
     * 支付成功后删除商品
     */
    public int deleteGood(@Param("g_id") int g_id);
}

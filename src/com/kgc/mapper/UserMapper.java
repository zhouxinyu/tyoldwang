package com.kgc.mapper;


import org.apache.ibatis.annotations.Param;

import com.kgc.entity.User;

public interface UserMapper {
	
	public User login(@Param("phone") String phone,@Param("password") String password);
	
	public User queryByPhone(@Param("phoneNum")String phone);
	
	public int addUser(@Param("user") User user);
	
	public User getNameByG_id(@Param("g_id")int g_id);
	
	public int UpdatePwd(@Param("phone")String phone,@Param("oldpassword")String oldpassword,@Param("newpassword")String newpassword);
	
	public int yzPhone(@Param("phone") String phone);
	
	public int updatePaswd(@Param("pwd") String pwd,@Param("phone") String phone);
	
	public int update(@Param("user")User user);
}

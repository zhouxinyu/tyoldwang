package com.kgc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kgc.entity.Goods;

public interface SeachMapper {
	
	List<Goods> queryBySort(@Param("sort_id")int sort_id);
	
}

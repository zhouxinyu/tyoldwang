<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>忘记密码</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link type="text/css" href="css/css.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
</head>
 <script type="text/javascript">
		$(function() {
			$("#submits").click(function(){
				var phone = $("#phone").val();
				var code = $("#code").val();
				$.ajax({
					url : "user/yz",
					type : "post",
					data : {
						"phone" : phone,
						"code" : code,
					},
					dataType : "JSON",
					success : function(data) {
						if (data != null && data == '11') {
								window.location.href="<%=request.getContextPath()%>/forgetpwd/forgetpwd3.jsp?phone="+phone;
						} else if (data != null && data == '22') {
	 						alert("该用户不存在或验证码错误");
						}
					}
					});
					
			})
		});
	</script>
<body>

  <div class="content">
   <div class="web-width">
     <div class="for-liucheng">
      <div class="liulist for-cur"></div>
      <div class="liulist"></div>
      <div class="liulist"></div>
      <div class="liutextbox">
       <div class="liutext for-cur"><em>1</em><br /><strong>验证手机号</strong></div>
       <div class="liutext"><em>2</em><br /><strong>设置新密码</strong></div>
       <div class="liutext"><em>3</em><br /><strong>完成</strong></div>
      </div>
     </div><!--for-liucheng/-->
     <form method="get" class="forget-pwd">
       <dl>
        <dt>验证方式：</dt>
        <dd>
         <select class="selyz">
          <option value="0">验证手机</option>
         </select>
        </dd>
        <div class="clears"></div>
       </dl>
       <dl>
        <dt>手机号：</dt>
        <dd><input type="text" value="<%=request.getParameter("phone")%>" id="phone" name="phone" /></dd>
        <div class="clears"></div>
       </dl>
       <dl>
        <dt>手机校验码：</dt>
        <dd><input type="text" id="code" name="code"/> <button id="yzm">获取短信验证码</button></dd>
        <div class="clears"></div>
       </dl>
       <div class="subtijiao"><input type="submit" value="提交" id="submits"/></div> 
      </form><!--forget-pwd/-->
   </div><!--web-width/-->
  </div><!--content/-->
  <script type="text/javascript">
		$(function() {
 			 $("#yzm").click(function(){
 			  var phone = $("#phone").val();
	 			 $.ajax({
						url : "user/yzPhone",
						type : "post",
						data : {
							"phone" : phone,
						},
						dataType : "JSON",
						success : function(data) {
							if (data != null && data == '11') {
		 						  $.getJSON("user/code", {
											"phone": phone
									  }, function(data) {
										if(data) {
											alert("验证码已发送!");
										} else {
											alert("验证码发送失败！");
										}
									});
							} else if (data != null && data == '22') {
		 						alert("该用户不存在");
							}
						}
						});
          })
       });
	</script>
  </body>
</html>

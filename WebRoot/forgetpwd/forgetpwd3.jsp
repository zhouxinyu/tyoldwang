<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>忘记密码</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link type="text/css" href="css/css.css" rel="stylesheet" />
	>
</head>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<body>

  <div class="content">
   <div class="web-width">
     <div class="for-liucheng">
      <div class="liulist for-cur"></div>
      <div class="liulist for-cur"></div>
      <div class="liulist"></div>
      <div class="liutextbox">
       <div class="liutext for-cur"><em>1</em><br /><strong>验证手机号</strong></div>
       <div class="liutext for-cur"><em>2</em><br /><strong>设置新密码</strong></div>
       <div class="liutext"><em>3</em><br /><strong>完成</strong></div>
      </div>
     </div><!--for-liucheng/-->
     <form  method="get" class="forget-pwd"> 
     	 <dl>
        <dt>手机号：</dt>
        <dd><input type="text" name="phone" value="<%=request.getParameter("phone") %>" id="phone"/></dd>
        <div class="clears"></div>
       </dl> 
       <dl>
        <dt>新密码：</dt>
        <dd><input type="password" id="password1" name="password1" placeholder="长度在6-18之间"/> <div class="clears" style="display: inline" id="tip2"></div></dd> 
		    <div class="clears"></div>
       </dl> 
       <dl>
        <dt>确认密码：</dt>
        <dd><input type="password" id="password2" name="password2" placeholder="长度在6-18之间"/> <div class="clears" style="display: inline" id="tip3"></div></dd>
		    <div class="clears"></div>
       </dl> 
       <div class="subtijiao" ><input type="submit" id="btn" value="提交" /></div> 
      </form><!--forget-pwd/-->
   </div><!--web-width/-->
  </div><!--content/-->
  <script>
    $(document).ready(function(){   
          $("#password1").blur(function(){
              var num=$("#password1").val().length;
              if(num<6){
                   $("#tip2").html("<font color=\"red\" size=\"2\">  密码太短</font>");
              }
              else if(num>18){
                   $("#tip2").html("<font color=\"red\" size=\"2\">  密码太长</font>");   
              }
              else{
                  $("#tip2").html("<font color=\"green\" size=\"2\"> 密码符合要求</font>");         
              }
          }) ;
          $("#password2").blur(function(){
              var tmp=$("#password1").val();
              var num=$("#password2").val().length;
              if($("#password2").val()!=tmp){
                  $("#tip3").html("<font color=\"red\" size=\"2\">  与第一次密码不相等</font>");    
              }
              else{
                  if(num>=6&&num<=18){
                      $("#tip3").html("<font color=\"green\" size=\"2\">  密码相同</font>");    
                  }                 
                  else{
                      $("#tip3").html("<font color=\"red\" size=\"2\">  无效的</font>");     
                  }                
              }
          });    
          
		  $("#btn").click(function(){
		  		var phone = $("#phone").val();
				var pwd = $("#password1").val();
				var pwd1 = $("#password2").val();
				if( (pwd.length > 5 && pwd.length < 19) &&  pwd1 == pwd){
					$.ajax({
						url : "user/updatePaswd",
						type : "post",
						data : {
							"phone" : phone,
							"pwd"  : pwd,
						},
						dataType : "json",
						success : function(data) {
							if(data){
								window.location.href="<%=request.getContextPath()%>/forgetpwd/forgetpwd4.jsp";
							}else{
								alert("密码修改失败");
							}
						}
					});
				}else{
					alert("密码填写不符合规范");
				}
		  })
        });
        </script>
</body>
</html>

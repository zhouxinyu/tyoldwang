<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<style type="text/css">
 
#books {
	margin: 100px auto;
}
#head {
	text-align:center;
	background-color: #11cd6e;
}
h1{
	font-size:30px;
}
tr>td:first-child{
	 text-align:center;
     width:150px; 
     height:70px;
     font-size:18px;
}
tr>td:last-child{
     width:450px;
     font-size:18px;
}
.button{
	border-spacing:10px 50px;
	background-color: #11cd6e;
	width: 100px;
	height: 40px;
}
input{
	font-size:18px;
	width:250px;
	height:30px;
}
</style>
<script src="http://www.jq22.com/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/distpicker.data.js"></script>
	  <script src="js/distpicker.js"></script>
  <link rel="stylesheet" href="css/public.css" />
   </head>
  <body>
  <script type="text/javascript">
  	$(function(){
  		var u_id = <%=request.getParameter("u_id") %>;
  		$.getJSON("Address/Info",{"u_id":u_id},function(data){
			$(data).each(function(){
				$("#name").val(this.gad_name);
				$("#phone").val(this.gad_phone);
				$("#adress").val(this.gad_address);
			})
			
		})
			
  		$("#submit").click(function(){
	  		var in_phone = $("#phone").val();
	  		var in_name = $("#name").val();
	  		var in_money = <%=request.getParameter("in_money") %>;
	  		var u_id = <%=request.getParameter("u_id") %>
	  		var g_id = <%=request.getParameter("g_id") %>
	  		var adress = $("#adress").val();
	  		if(!(/^1[3456789]\d{9}$/.test(in_phone))) {
					alert("电话号码格式不正确");
			} else{
				if(in_phone!="" && in_name!="" && adress!=""){
		  		window.location.href = "pay/to_alipay?adress="+adress+"&&in_money=" + in_money +"&&u_id="+u_id+"&&g_id="+g_id
		  		+"&&in_phone="+in_phone+"&&in_name="+in_name;
		  		}
		  		else{
		  			alert("请完整填写资料");
		  		}
			}
  		});
  	})
  </script>
  <jsp:include page="header.jsp"></jsp:include>
   <form>
		<table id="books" border="1">
		<tr>
			<td colspan="2" id="head" height="100px"><h1>编辑收货地址</h1></td>
		</tr>
				<tr>
					<td>姓名<span style="color: red;">(*)</span></td>
					<td><input type="text" name="name" id="name" value=""/></td>
				</tr>
				<tr>
					<td>手机号<span style="color: red;">(*)</span></td>
					<td><input type="text" name="phone" id="phone" value=""/></td>
				</tr>
				<tr>
					<td>详细地址<span style="color: red;">(*)</span></td>
					<td><input type="text" name="adress"" id="adress" value=""></td>
				</tr>
				<tr>
					<td>备注</td>
					<td><textarea style="resize:none;"></textarea>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="button" value="提交" id="submit" class="button"/>
						<input type="button" value="返回" class="button"  onclick="javascript:window.history.back(-1)"/>
					</td>
				</tr>
			</table>
		</form>
		<jsp:include page="footer.jsp"></jsp:include>
  </body>
</html>

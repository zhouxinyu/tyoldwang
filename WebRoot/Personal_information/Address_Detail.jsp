
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>我的地址</title>
<!--格式-->
<link
	href="//g.alicdn.com/tb/mtb-profile/0.0.2/oth/p/sns/1.0/tbsp-sns-min.css?t=20120401.css"
	type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../css/public.css" />
<link rel="stylesheet" href="../css/layui.css" />
<link rel="stylesheet" href="../../css/new_file.css" />

<style>
#privacySet {
	display: none
}

#page {
	margin-top: 40px;
	position: relative;
}

.mt-menu {
	margin-left: 30px;
}

.address_information {
	height: 170px;
	width: 300px;
	border: 1px solid gray;
	float: left;
	margin-right: 30px;
	margin-bottom: 30px;
	display: block;
}

.address_addadd {
	height: 170px;
	line-height: 170px;
	text-align: center;
	width: 300px;
	border: 1px solid gray;
	display: block;
	margin-bottom: 30px;
	clear: left;
}

.address_shang {
	height: 70px;
	width: 300px;
}

.address_name {
	height: 30px;
	line-height: 50px;
	width: 300px;
	font-size: 18px;
	text-indent: 2em;
}

.address_tel {
	height: 30px;
	line-height: 50px;
	width: 300px;
	font-size: 14px;
	text-indent: 2.5em;
}

.address_xia {
	height: 70px;
	width: 300px;
	text-align: left;
}

.address_add {
	height: 70px;
	line-height: 40px;
	width: 300px;
	font-size: 16px;
	text-indent: 2em;
}

.clear {
	clear: right;
}

a {
	color: orange;
}

a:hover {
	color: red;
}

.tianjiabiaoge {
	width: 800px;
	height: 600px;
	position: absolute;
	z-index: 10;
	left: 15%;
	top: -5%;
	background: white;
	display: none;
}

.add_tab {
	margin-left: 10%;
	margin-top: 10%;
	height: 500px;
	width: 600px;
}

.add_tab tr {
	text-align: center;
	width: 600px;
	height: 100px;
}

.add_tab tr td {
	font-size: 18px;
	width: 150px;
}

.add_input {
	width: 400px;
}

input {
	width: 150px;
	height: 100px;
	border: 0px;
}

.shanchu {
	width: 300px;
	height: 40px;
	margin-top: -7px;
	background: #90ee90
}

.tijiao {
	height: 50px;
	background: #90ee90;
}

.quxiao {
	height: 50px;
	background: #90ee90;
}
</style>
<script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/location.js"></script>
<script type="text/javascript">
	$(function() {
		$(".address_addadd").click(function() {
			$(".tianjiabiaoge").css("display", "block");
		})
		$(".tijiao").click(function() {
			var u_id = '${sessionScope.user.u_id}';
			var u_name = '${sessionScope.user.u_name}';
			var u_password = '${sessionScope.user.u_password}';
			var gad_name = $("input[name=name]").val();
			var gad_phone = $("input[name=tel]").val();
			var gad_address = $("#province option:selected").text() + $("#city option:selected").text() + $("#county option:selected").text() + $("input[name=add_detail]").val();
			var gad_sex = $(".sex").val();
			if (gad_sex == "女士") {
				gad_sex = 0;
			} else if (gad_sex == "先生") {
				gad_sex = 1;
			}
			var address = {
				"u_id" : u_id,
				"u_name" : u_name,
				"u_password" : u_password,
				"gad_name" : gad_name,
				"gad_phone" : gad_phone,
				"gad_address" : gad_address,
				"gad_sex" : gad_sex
			}
			// 添加地址
			$.ajax({
				url : "../Address/insert",
				type : "post",
				data : address,
				async : false,
				datatype : "json",
				success : function(data) {
					if (data) {
						alert("地址添加成功");
						$(".tianjiabiaoge").css("display", "none");
						window.location.href = "Address_Detail.jsp";
					}
				}
			})

		})
		$.getJSON("../Address/get", {
			"u_id" : '${sessionScope.user.u_id}'
		}, function(data) {

			var str = "";
			$(data).each(function() {
			if(this.gad_sex==0){this.gad_sex = "女士";}else if(this.gad_sex==1){this.gad_sex = "先生";}
				str += "<div class='address_information'><div class='address_shang'><div class='address_name'>" + this.gad_name + "&nbsp;&nbsp;<span>" + this.gad_sex + "</span></div>";
				str += "<div class='address_tel'>电话：" + this.gad_phone + "</div></div>";
				str += "<div class='address_xia'><div class='address_add'>" + this.gad_address + "</div><input type='button' value='删除地址' onclick=\"deleteadd("
					+ this.gad_id + ")\" class='shanchu'/></div></div>";
			})
			$(".address_addadd").before(str);

		})
		$(".quxiao").click(function() {
			if (confirm("确认取消添加地址吗？")) {
				window.location.href = "Address_Detail.jsp";
			}
		})
	})
	function deleteadd(gad_id) {
		if (confirm("确认删除地址嘛？")) {
			$.ajax({
				url : "../Address/delete",
				type : "post",
				data : {
					"gad_id" : gad_id
				},
				async : false,
				datatype : "json",
				success : function(data) {
					if (data) {
						alert("地址删除成功！");
						window.location.href = "Address_Detail.jsp";
					} else {
						alert("地址删除失败！");
					}
				}
			})


		/* $.getJSON("../Address/delete", {
			"gad_id" : gad_id
		}, function(data) {
			if (data) {
				alert("地址删除成功！");
			} else {
				alert("地址删除失败！");
			}
		}); */
		}
	}
</script>
</head>

<body class="mission  mytaobao-v2 ">
	<%@ include file="header.jsp"%>
	<div id="page">
		<link rel="stylesheet"
			href="//g.alicdn.com/tb/mtbframe/2.0.2/pages/home/base.css">
		<script type="text/javascript"
			src="//g.alicdn.com/tb/mtbframe/2.0.4/components/common/base.js"></script>

		<div id="content" class="layout grid-s160m0">
			<div id="mytaobao-panel" class="grid-c2">
				<link href="//g.alicdn.com//tb/mtb-profile/0.0.2/app-config.css"
					type="text/css" rel="stylesheet" />

				<div class="col-main">
					<div class="main-wrap">
						<div id="profile" class="sns-config">
							<div class="sns-tab tab-app">
								<ul>
									<li class="selected">
										<h3>收货地址</h3>
									</li>
								</ul>
								<ul class="tab-sub">
									<li class="selected"><a
										href="pages/Address/Address_Detail.jsp"><span>收货地址</span></a>
								</ul>
							</div>
							<div class="sns-box box-detail">
								<div class="bd">
									<div class="sns-nf">
										<div id="main-profile" class="parts">
											<!--内容-->

											<div class="address_addadd">
												<a>＋添加新地址</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="col-sub" style="z-index:0">
					<aside class="mt-menu" id="J_MtSideMenu">
						<div class="mt-menu-tree">
							<dl class="mt-menu-item mt-account-manage no-decoration">
								<dt>账号管理</dt>
								<dd>
									<!--头像-->
									<img src="../${sessionScope.user.u_touxiang}"
										style="width: 60px;height:60px;">
								</dd>
								<dd>
									<a href="Personal_information.jsp">个人资料</a>
								</dd>
								<dd>
									<a href="UpdatePwd.jsp">修改密码</a>
								</dd>
								<dd>
									<a href="AllOrder.jsp">我的订单</a>
								</dd>
								<dd>
									<a href="Address_Detail.jsp" style="color: red;">收货地址</a>
								</dd>
								<dd>
									<a href="Assess.jsp">我的商品</a>
								</dd>
							</dl>
						</div>
					</aside>
				</div>
				<div class="tianjiabiaoge">
					<form action="#">
						<table border="1px solid" class="add_tab">
							<tr>
								<td>姓名</td>
								<td class="add_input"><input type="text" name="name" /></td>
								<td>性别</td>
								<td class="add_input"><select class="sex">
										<option>先生</option>
										<option>女士</option>
								</select></td>
							</tr>
							<tr>
								<td>电话</td>
								<td colspan="3" class="add_input"><input type="tel"
									name="tel" style="width: 450px;" /></td>
							</tr>
							<tr>
								<td>收货地址</td>
								<td><select id="province" style="width: 100px;">
								</select></td>
								<td><select id="city" style="width: 100px;">
								</select></td>
								<td><select id="county" style="width: 100px;">
								</select></td>
							</tr>
							<tr>
								<td>详细地址</td>
								<td colspan="3" class="add_input"><input type="text"
									name="add_detail" style="width: 450px;" /></td>
							</tr>
							<tr>
								<td colspan="4"><input type="button" value="提交"
									class="tijiao" /> <input type="button" value="取消"
									class="quxiao" /></td>
							</tr>
						</table>
					</form>

				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-wrap">
		<div class="footer-bottom">
			<p class="friend-link">
				<span>友情链接：</span>
				<!-- <a href="">淘二淘</a> -->

				<a href="http://wanlinqiang.com?from=taoertao" target="_blank">万林强的博客</a>

				<a href="http://www.taoertao.com" target="_blank">校园二手街</a> <a
					href="https://portal.qiniu.com/signup?code=3latfmv9iksb6"
					target="_blank">七牛云</a> <a href="http://www.taoertao.com"
					target="_blank">大学生二手网</a> <a
					href="https://www.vultr.com/?ref=7549292" target="_blank">免备案服务器</a>

				<a href="http://www.jpfuli.com?from=taoertao" target="_blank">极品福利</a>

				<a href="http://vip.sucai.tv/" target="_blank">免费VIP视频</a> <a
					href="http://www.mosenx.com/?from=taoertao" target="_blank">墨森运动</a>

			</p>
			<p class="column">
				<a href="/service/about">关于我们</a> <a href="/service/problem">常见问题</a>
				<a href="/user/help">意见反馈</a> <a href="/service/protocol">服务协议</a> <a
					href="/service/contect">联系我们</a>
				<script
					src="http://s95.cnzz.com/stat.php?id=1255800214&web_id=1255800214"
					language="JavaScript"></script>
				<script>
					var _hmt = _hmt || [];
					(function() {
						var hm = document.createElement("script");
						hm.src = "https://hm.baidu.com/hm.js?b43531d7c229bad3bcbfbc7991208c60";
						var s = document.getElementsByTagName("script")[0];
						s.parentNode.insertBefore(hm, s);
					})();
				</script>
			</p>
			<p class="tips">本站所有信息均为用户自由发布，本站不对信息的真实性负任何责任，交易时请注意识别信息的真假如有网站内容侵害了您的权益请联系我们删除，举报QQ：584845663</p>
			<!--<p><span>举报QQ：584845663</span>　<span>商务邮箱：584845663@qq.com</span>　<script src="http://s95.cnzz.com/stat.php?id=1255800214&web_id=1255800214" language="JavaScript"></script></p>-->
			<p class="right">
				<span>Copyright © 2014-2017, Taoertao.com, All Rights
					Reserved</span> <a target="_blank" href="http://www.miitbeian.gov.cn/">浙ICP备16002812号</a>
				<a class="beian" target="_blank"
					href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=33011802000633"
					style="display:inline-block;height:20px;line-height:20px;">浙公网安备
					33011802000633号</a> <span>商务邮箱：584845663@qq.com</span> <a
					href="http://webscan.360.cn/index/checkwebsite/url/new.taoertao.com"
					target="_blank"><img border="0"
					src="http://webscan.360.cn/img/logo_verify.png" /></a>
			</p>
		</div>
	</div>
</body>

</html>
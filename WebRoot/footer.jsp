<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div id="footer">
	<div class="go-to-top">
		<a href="#" title="回顶部" class="icon">&#xe61c;</a>
	</div>
	<div class="footer-bottom-wrap">
		<div class="footer-bottom">
			<p class="friend-link">
				<span>友情链接：</span>
				<!-- <a href="">淘二淘</a> -->

				<a href="#" >周大宝的博客</a> <a href="#" >校园二手街</a>

				<a href="#" >七牛云</a> <a href="#" >大学生二手网</a>

				<a href="#" >免备案服务器</a> <a href="#" >极品福利</a>

				<a href="#" >免费VIP视频</a> <a href="#" >墨森运动</a>

			</p>
			<p class="column">
				<a href="#">关于我们</a> <a href="#">常见问题</a>
				<a href="#">意见反馈</a> <a href="#">服务协议</a> <a
					href="#">联系我们</a>
			</p>
			<p class="tips">本站所有信息均为用户自由发布，本站不对信息的真实性负任何责任，交易时请注意识别信息的真假如有网站内容侵害了您的权益请联系我们删除，举报QQ：584845663</p>
			
			<p class="right">
				<span>Copyright © 2018-2019, xianyu.com, All Rights
					Reserved</span> <a href="#">苏ICP备16723894号</a>
				<a class="beian" target="_blank"
					href="#"
					style="display:inline-block;height:20px;line-height:20px;">苏公网安备
					33011802000633号</a> <span>商务邮箱：1234567898@qq.com</span> <a
					href="#"
					><img border="0"
					src="http://webscan.360.cn/img/logo_verify.png" /></a>
			</p>
		</div>
	</div>
</div>


<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="header">
	<div class="header-wrap">
		<a href="" class="logo fl"> <img src="img/new-logo.png"></a>
		<ul class="nav fl">
			<li><a href="shouye.jsp">首页</a></li>
			<li><a href="Personal_information/Personal_information.jsp" class="yhq">个人信息</a></li>
			<li><a href="collect.jsp">购物车</a></li>
			<li><a href="contact.jsp">联系我们</a></li>
		</ul>
		<script type="text/javascript">
			$(function(){
				$.ajaxSettings.async = false;
				$.getJSON("user/getcookies",function(data){
					var flag = data;
					if(flag){
						window.location.reload();
					}
				});
			});
			
		</script>
		<div class="nav-right fr" id="isLogin">
			
			<c:choose>
				<c:when test="${sessionScope.user.u_name != null}">
					<a href="Personal_information/Personal_information.jsp" class="log-btn"><img src="${sessionScope.user.u_touxiang}" class="user-header-image" />${sessionScope.user.u_name}</a><a class="log-btn"
					 href="user/exitUser">退出</a>
				</c:when>
				<c:otherwise>
					<a href="register.jsp" class="log-btn">注册</a> <a class="log-btn" href="login.jsp">登录</a>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>

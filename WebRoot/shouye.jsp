<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商城首页</title>
<link rel="stylesheet" href="css/public.css" />
<link rel="stylesheet" href="css/index.css" />
<link rel="stylesheet" href="css/layui.css" />
<link rel="stylesheet" href="css/reset(fenye).css" />
<link rel="stylesheet" href="css/pagination.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<script type="text/javascript" src="js/highlight.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/index.js"></script>
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div id="header-bottom">
		<input type="hidden" id="userName" value='<%= session.getAttribute("userName")%>'/>
		<div class="header-bottom-wrap clearfix">
			<div class="city fl">
				<a href="javascript:;">校园<i class="icon"></i></a>
			</div>
			<div class="search-wrap fl">
				<div class="search">
					<input type="text" class="keywords" id="search1" placeholder="二手市场交易" value=""> 
					<a href="" id="search" class="search-btn search-normal-btn">搜索</a>
<script type="text/javascript">
	$(function(){
		$("#search").click(function(){
			var name = $("#search1").val();
			if(name!=""){
			$("#search").attr("href","search.jsp?name="+name);
			}else{
				$("#search").attr("href","search.jsp?name=null");
			}
		})
		
	
	})
</script>
				</div>
			</div>

			<div class="publish fr">
				<a href="publish.jsp" class="publish-btn"><i class="icon"></i>发布商品</a>
			</div>
		</div>
	</div>
	<div id="main" class="clearfix">
		<!-- *****************分类***************************** -->
		<div class="category fl">
			<ul id="goSort"></ul>
		</div>
		<div class="main-box fl">
			<div class="banner">
				<div class="focusMap fl">
					<span class="prev" style="display: none;"><i class="icon"></i></span>
					<span class="next" style="display: none;"><i class="icon"></i></span>
					<ul class="rImg">

						<li style="display: none;"><a href="#" ><img
								src="img/head1.jpg"></a></li>

						<li style="display: none;"><a href="#" ><img
								src="img/head2.jpg"></a></li>

						<li style="display: list-item;"><a href="#" ><img
								src="img/head3.jpg" ></a></li>

					</ul>

					<ul class="button">

						<li class=""></li>

						<li class=""></li>

						<li class="on"></li>

					</ul>

				</div>
			</div>
			<div class="index-list">
				<div class="list-header">
					<a href="#" class="active">推荐</a>
					<a href="publish.jsp" class="fr"><i class="icon"></i>发布信息</a>
				</div>

				<div class="list-body">
					<!--  *******************************jquery导入********************************************** -->
					<ul class="clearfix" id="clearfix"></ul>
				</div>
				<div id="page">
					<div class="m-style M-box11"></div>
				</div>
			</div>
		</div>
		<div class="sidebar fr">
			<div class="sidebar-banner-right">
				<div class="wei clearfix">
					<div class="wei-left fl">
						<a href="javascript:;" class="weixin"><img
							src="img/weixin.png"></a> <a
							href="javascript:;"  class="weibo"><img
							src="img/weibo.png"></a>
					</div>
					<div class="wei-right fr">
						<img src="img/weixin1.jpg" alt=""> 
					</div>
				</div>
				<div class="app-download">
					<p>手机 APP 下载</p>
					<a onclick="app()">Download</a>
					<script type="text/javascript">
						function app(){
							alert("暂未开放");
						}
					</script>
				</div>
				 <ul class="jizhuan-list">
					<li>
                        <a class="jizhuan" target="_blank">
                            <img class="jizhuan-image" src="https://img.alicdn.com/tfscom/i1/2243773493/TB2QKeIFbSYBuNjSspfXXcZCpXa_!!2243773493-0-item_pic.jpg" alt="男士内裤男平角裤纯棉莫代尔冰丝学生青年四角裤头韩版个性潮夏季">
                            <p class="jizhuan-title">男士内裤男平角裤纯棉莫代尔冰丝学生青年四角裤头韩版个性潮夏季</p>
                            </a><p><a class="jizhuan" target="_blank">
								<span class="yhq">￥26.80</span>
								<span class="jizhuan-price">￥168.00</span>
                                </a><a target="_blank" title="满24元减5元" class="quan">领券</a>
							</p>
                    </li>
					<li>
                        <a class="jizhuan" target="_blank">
                            <img class="jizhuan-image" src="https://img.alicdn.com/tfscom/i1/2124921418/O1CN01qR8Yeu1MLUBiV2cxo_!!0-item_pic.jpg" alt="男童运动裤春秋款2019冬季厚中大童裤子15岁12儿童长裤加绒胖男孩">
                            <p class="jizhuan-title">男童运动裤春秋款2019冬季厚中大童裤子15岁12儿童长裤加绒胖男孩</p>
                            </a><p><a class="jizhuan" target="_blank">
								<span class="yhq">￥59.00</span>
								<span class="jizhuan-price">￥109.00</span>
                                </a><a target="_blank" title="满9元减5元" class="quan">领券</a>
							</p>
                    </li>
					<li>
                        <a class="jizhuan" target="_blank">
                            <img class="jizhuan-image" src="https://img.alicdn.com/tfscom/i2/2453089709/O1CN01qUwl742Lalw72U0WJ_!!0-item_pic.jpg" alt="20000毫安充电宝大容量超薄小巧便携适用小米苹果vivo华为冲手机通专用移动电源快闪充石墨烯女1000000超大量">
                            <p class="jizhuan-title">20000毫安充电宝大容量超薄小巧便携适用小米苹果vivo华为冲手机通专用移动电源快闪充石墨烯女1000000超大量</p>
                            </a><p><a class="jizhuan" target="_blank">
								<span class="yhq">￥49.90</span>
								<span class="jizhuan-price">￥118.00</span>
                                </a><a title="满49元减25元" class="quan">领券</a>
							</p>
                    </li>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	<script type="text/javascript">
		
		var prov = 1;
			/* 首页初始化,同步执行*/
			$.ajaxSettings.async = false;
			$.getJSON("goods/main", function(data) {
				var list = data.lists;
				var str = "";
				for (var i = 0; i < list.length; i++) {
					str += "<li><a href='#' target='_blank' class='good-image goods' value='"+list[i].g_id+"'><img class='image-show-box' src='" + list[i].g_img + "'>" +
						"</a><a href='#' target='_blank' class='good-title goods' value='"+list[i].g_id+"'>" + list[i].g_name + "</a>" +
						"<span class='good-price'>" + list[i].g_price + "</span><span class='pub-time fr'>发布于 " + list[i].create_time + "</span></li>";
				}
				<%-- console.log(str);
				alert(<%=session.getAttribute("totalPage")%>); --%>
				$("#clearfix").html(str);
			});
		
		$(function() {
		
			/* 初始列表*/
			$.getJSON("publish/getSorts",function(data){
				var str="";
				$(data).each(function(){
					str+="<li><a href='' class='sort' value='"+this.sort_id+"'>"+this.sort_name+"<i class='icon'>></i></a></li>";
				})
				$("#goSort").html(str);
			});
			
			
			
			$(".M-box11").pagination({
				mode : 'fixed',
				pageCount : <%=session.getAttribute("totalPage")%>
			});
			
			/* 上下页 */
			$(".M-box11").click(function() {
				var next = $(".M-box11 span").html();
				if (next != prov) {
					$.getJSON("goods/main2",{"currentPage":next}, function(data) {
						var list = data;
						var str = "";
						for (var i = 0; i < list.length; i++) {
							str += "<li><a href='shopshow.jsp' target='_blank' class='good-image goods' value='"+list[i].g_id+"'><img class='image-show-box' src='" + list[i].g_img + "'>" +
								"</a><a href='/detail/1931' target='_blank' class='good-title goods' value='"+list[i].g_id+"'>" + list[i].g_name + "</a>" +
								"<span class='good-price'>" + list[i].g_price + "</span><span class='pub-time fr'>发布于 " + list[i].create_time + "</span></li>";
						}
						$("#clearfix").html(str);
					});
					prov = next;
				} 
			});
			
			$("#clearfix").on("click",".goods",function(){
				var g_id = $(this).attr("value");
				window.location.href="shopshow.jsp?g_id="+g_id;
				window.event.returnValue=false; 
			});
			
			$("#goSort").on("click",".sort",function(){
				var sort_id = $(this).attr("value");
				window.location.href="sortgoods.jsp?sort_id="+sort_id;
				window.event.returnValue=false; 
			});
			
		});
		
		/* function goSort(){
			window.location.href="sortgoods.jsp";    
   			window.event.returnValue=false; 
		} */
	</script>
</body>
</html>